Simbelmynë
==========

.. include:: description.rst


.. toctree::
   :maxdepth: 1
   :caption: Getting started

   gettingstarted/installation
   gettingstarted/examples
   
.. toctree::
   :maxdepth: 1
   :caption: Usage

   usage/parameterfile
   usage/structures
   
.. toctree::
    :maxdepth: 1
    :caption: Developer documentation

..    developer/architecture
..    developer/contributing
   
   
.. include:: reference.rst


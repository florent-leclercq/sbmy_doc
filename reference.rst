Reference
---------

Simbelmynë does not have its scientific paper yet. For the time being, if you want to acknowledge use of this software, please cite |Leclercqetal2015b|, where it was first used:

.. |Leclercqetal2015b| raw:: html

   <a href="https://arxiv.org/abs/1502.02690" target="blank">Leclercq <i>et al.</i> 2015b</a>

| *Bayesian analysis of the dynamic cosmic web in the SDSS galaxy survey*
| F. Leclercq, J. Jasche, B. Wandelt
| |Reference|

.. |Reference| raw:: html

    <a href="http://dx.doi.org/10.1088/1475-7516/2015/06/015" target="blank">JCAP <b>6</b>, 15 (2015)</a>, <a href="http://arxiv.org/abs/1502.02690" target="blank">arXiv:1502.02690</a> [<a href="http://arxiv.org/abs/1502.02690" target="blank">astro-ph.CO</a>] [<a href="https://ui.adsabs.harvard.edu/?#abs/2015JCAP...06..015L" target="blank">ADS</a>] [<a href="http://arxiv.org/pdf/1502.02690" class="document" target="blank">pdf</a>]

.. code-block:: console

    @ARTICLE{2015JCAP...06..015L,
        author = {{Leclercq}, Florent and {Jasche}, Jens and {Wandelt}, Benjamin},
        title = "{Bayesian analysis of the dynamic cosmic web in the SDSS galaxy survey}",
        journal = {Journal of Cosmology and Astro-Particle Physics},
        keywords = {Astrophysics - Cosmology and Nongalactic Astrophysics},
        year = 2015,
        month = Jun,
        volume = {2015},
        eid = {015},
        pages = {015},
        doi = {10.1088/1475-7516/2015/06/015},
        archivePrefix = {arXiv},
        eprint = {1502.02690},
        primaryClass = {astro-ph.CO},
        adsurl = {https://ui.adsabs.harvard.edu/\#abs/2015JCAP...06..015L},
        adsnote = {Provided by the SAO/NASA Astrophysics Data System}
    }



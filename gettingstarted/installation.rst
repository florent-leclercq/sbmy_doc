Installation
============

This page contains instructions for installing the **Simbelmynë** code.

Installing the dependencies
---------------------------

Mandatory dependencies
~~~~~~~~~~~~~~~~~~~~~~

The code requires the following software packages:

 - |hdf5| (version 1.10 or higher)
 - |fftw3| (version 3.3.6 or higher)
 - |gsl| (version 2.3 or higher)
 - python 3, as provided for example by |anaconda3|, with classic extensions such as NumPy, Matplotlib, etc.

.. |hdf5| raw:: html

   <a href="https://support.hdfgroup.org/HDF5/" target="blank">hdf5</a>
   
.. |fftw3| raw:: html

   <a href="http://www.fftw.org/" target="blank">fftw</a>

.. |gsl| raw:: html

   <a href="https://www.gnu.org/software/gsl/" target="blank">gsl</a>

.. |anaconda3| raw:: html

   <a href="https://www.anaconda.com/download/#linux" target="blank">anaconda3</a>

The mandatory dependencies should be compiled using the following flags:

**gsl**

.. code-block:: bash

    ./configure --prefix=PATH/TO/gsl
    make
    make install

**fftw**

.. code-block:: bash

    ./configure --prefix=PATH/TO/fftw --enable-openmp --enable-shared
    make
    make install
    ./configure --prefix=PATH/TO/fftw --enable-openmp --enable-shared --enable-float
    make
    make install

.. Warning:: Make sure to install *both* the double-precision version (first part) and the float-precision version (second part).
    
**hdf5**

.. code-block:: bash

    ./configure --prefix=PATH/TO/hdf5 --enable-shared
    make
    make install


**anaconda3**

Anaconda3 (see its documentation) comes with a bash installer, for instance:

.. code-block:: bash

    bash Anaconda3-5.2.0-Linux-x86_64.sh

In case of compiler version conflict one may have to remove anaconda's gcc, and it may be necessary to install packages such as matplotlib:

.. code-block:: bash

    conda remove libgcc
    conda install matplotlib


Setting the environment variable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As the code links the dynamic libraries at compilation, you need to add the libraries' paths to your ``LD_LIBRARY_PATH`` before running:

.. code-block:: bash

    # add some libraries to $LD_LIBRARY_PATH
    export LD_LIBRARY_PATH=PATH/TO/gsl/lib:$LD_LIBRARY_PATH
    export LD_LIBRARY_PATH=PATH/TO/fftw/lib:$LD_LIBRARY_PATH
    export LD_LIBRARY_PATH=PATH/TO/hdf5/lib:$LD_LIBRARY_PATH

For convenience, the code above can be added to your ``$HOME/.bashrc`` (or ``.bash_profile``, ``.profile``, ``.zshrc`` etc.).


Optional dependencies
~~~~~~~~~~~~~~~~~~~~~

The following dependencies are optional but recommended; they are necessary to some features of the Simbelmynë code:

* |CLASS| and its python wrapper - used as an option to produce the input power spectrum of simulations
* |healpy| - used anywhere projections on the sky are involved

.. |CLASS| raw:: html

   <a href="http://class-code.net" target="blank">CLASS</a>

.. |healpy| raw:: html

   <a href="https://github.com/healpy/healpy" target="blank">healpy</a>


.. Cloning the repositories

Cloning the repository
----------------------

.. Main repository
.. ~~~~~~~~~~~~~~~

Simbelmynë includes a main piece of code in the |principalrepo|, as well as several optional "extensions" (to be released).

.. |principalrepo| raw:: html

   <a href="https://bitbucket.org/florent-leclercq/simbelmyne/" target="blank">principal repository</a>

To download the main code, clone the repository using:

.. code-block:: bash

    git clone git@bitbucket.org:florent-leclercq/simbelmyne.git SBMY_ROOT/
    
where you can replace ``SBMY_ROOT/`` with your desidered installation directory.
    
.. Note:: Note that if you want to modify the code and/or some of the extensions, you shall first fork the corresponding repositories.


.. Extensions
.. ~~~~~~~~~~

.. Optional extensions of Simbelmynë (currently "|classifiers|" and "|dmsheet|") shall be placed in the directory ``SBMY_ROOT/extensions/``. To download them, use (for example):

.. |classifiers| raw:: html

   <a href="" target="blank"><i>classifiers</i></a>
   
.. |dmsheet| raw:: html

   <a href="" target="blank"><i>dmsheet</i></a>

.. .. code-block:: bash

    mkdir -p SBMY_ROOT/extensions/
    cd SBMY_ROOT/extensions/
    git clone git@bitbucket.org:florent-leclercq/sbmy_classifiers.git classifiers/


Adjusting the compilation options
---------------------------------

Go to ``SBMY_ROOT/`` and open ``config.mk`` to edit your compilation options. You can add a "``SYSTYPE``" for your machine and adapt (in particular) the ``LDFLAGS`` and ``INCLUDES`` to match the path where you have installed your dependencies above. The following template can be used.

.. code-block:: bash

    ifeq ($(SYSTYPE),"YourSystem")
        CC		= gcc
        RM		= rm -rf
        ECHO		= echo
        MKDIR		= @mkdir -p
        LDFLAGS		= -L PATH/TO/fftw/lib -L PATH/TO/gsl/lib -L PATH/TO/hdf5/lib
        LDLIBS		= -lm -lgsl -lgslcblas -lfftw3 -lfftw3_threads -lfftw3f -lfftw3f_threads -lhdf5
        INCLUDES	= -I PATH/TO/fftw/include -I PATH/TO/gsl/include -I PATH/TO/hdf5/include
        PYTHONFLAGS	= -fPIC -shared
        DEBUG		= "false"
    endif


The following options (optimization flags depending on the C-compiler that is used, and code verbose level) can also be adjusted:

.. code-block:: bash

    ifeq ($(DEBUG),"true")
        OPTIMIZE	= -fopenmp -g3 -ggdb -W -Wall -pedantic -pedantic-errors -std=c99
        OPT		+= -DDEBUG
        OPT		+= -DSCREEN_VERBOSE_LEVEL=7 -DLOGS_VERBOSE_LEVEL=7
    else
        OPTIMIZE	= -fopenmp -O3 -std=c99
        OPT		+= -DSCREEN_VERBOSE_LEVEL=4 -DLOGS_VERBOSE_LEVEL=6
    endif

Then, under "Select target computer and optimization mode", add ``SYSTYPE="YourSystem"`` and comment out the other lines.

A number of compilation-time options are contained in ``config.mk`` and will determine the types of simulations that can be run with a given version of Simbelmynë. These options are documented in the template provided. In principle, it should not be necessary to modify the ``makefile``.

.. Note:: If you are a Simbelmynë developer, please do not include your private version of ``config.mk`` in your pull requests.

.. To switch on optional extension(s), uncomment and complete the following line as necessary:

.. .. code-block:: bash

    # EXTENSIONS			= classifiers dmsheet


Recommended modifications of .bashrc
------------------------------------

To run smoothly, the code generally requires to increase the stack size. We therefore suggest to add the following to your ``$HOME/.bashrc`` (or ``.bash_profile``, ``.profile``, ``.zshrc`` etc.)

.. code-block:: bash

    # set stack size to unlimited
    ulimit -s unlimited


Compiling the C code
--------------------

Once everything is set up you should be able to compile:

.. code-block:: bash

    make

An executable (``build/simbelmyne``) and a shared object (``pysbmy/build/simbelmyne.so``) are produced.

Installing the python wrapper package (pysbmy)
----------------------------------------------

After successfully compiling the C code, install the python package using

.. code-block:: bash

    pip install .
    
The list of python packages required for the installation can be found in ``setup.py``.

Running the code
----------------

The code is run by using the entry point ``simbelmyne`` created by the python package, or by using directly the executable:

.. code-block:: bash

    simbelmyne PATH/TO/config.sbmy PATH/TO/logs.txt

or

.. code-block:: bash

    SBMY_ROOT/build/simbelmyne PATH/TO/config.sbmy PATH/TO/logs.txt

The first mandatory argument is the :doc:`Simbelmynë parameter file <../usage/parameterfile>` (which can be generated using python), the second optional argument is the path for the output log file. If the second argument is omitted then logs will not be saved.

Simbelmynë is a cosmological software package written in C and Python. It is a hierarchical probabilistic simulator to generate synthetic galaxy survey data.

For more information on the code's purpose and features, please see its |homepage|.

.. |homepage| raw:: html

   <a href="http://simbelmyne.florent-leclercq.eu/" target="blank">homepage</a>

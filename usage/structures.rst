Internal structures
===================

**Simbelmynë** uses internally several structures.

To each Simbelmynë structure corresponds a C-structure (defined in ``libSBMY/include/structures.h``) and a python class (found in ``pysbmy/*.py``). C and python reading/writing routines for HDF5 files are provided. The C reading/writing routines are defined in ``libSBMY/src/io.c`` and the python equivalents in ``pysbmy/*.py``.

Field structure
---------------

This structure is used for cosmological fields of rank :math:`r` (typically :math:`r=1` for a scalar field and :math:`r=3` for a vector field) defined on a 3D cartesian grid.

Content
~~~~~~~

The HDF5 file structure is:

* ``/info/scalars/L0`` (attribute, type ``double``): size of the box in Mpc/h
* ``/info/scalars/L1`` (attribute, type ``double``): size of the box in Mpc/h
* ``/info/scalars/L2`` (attribute, type ``double``): size of the box in Mpc/h
* ``/info/scalars/corner0`` (attribute, type ``double``): x-position in Mpc/h of the corner of the box with respect to the observer (which is at (0,0,0)).
* ``/info/scalars/corner1`` (attribute, type ``double``): y-position in Mpc/h of the corner of the box with respect to the observer (which is at (0,0,0)).
* ``/info/scalars/corner2`` (attribute, type ``double``): z-position in Mpc/h of the corner of the box with respect to the observer (which is at (0,0,0)).
* ``/info/scalars/N0`` (attribute, type ``int``): number of volume elements of the mesh
* ``/info/scalars/N1`` (attribute, type ``int``): number of volume elements of the mesh
* ``/info/scalars/N2`` (attribute, type ``int``): number of volume elements of the mesh
* ``/info/scalars/rank`` (attribute, type ``int``): rank of the field (usually 1 or 3)
* ``/info/scalars/time`` (attribute, type ``double``): time at which the field is represented
* ``/scalars/field`` (dataset, type ``4-byte float little-endian`` (``<f4``), dimensions (``N0``, ``N1``, ``N2``)): the field

Use in C
~~~~~~~~

In Simbelmynë C files, a Field structure can be read, allocated, written and freed using respectively:

.. code-block:: C

    Field F = read_field("field.h5")
    Field F = allocate_field(rank, N0, N1, N2, corner0, corner1, corner2, L0, L1, L2, Time);
    Field F = allocate_scalar_field(N0, N1, N2, corner0, corner1, corner2, L0, L1, L2, Time);
    write_field(F, "field.h5");
    free_field(F);

Use in python
~~~~~~~~~~~~~

With python, a field structure can be read/written using:

.. code-block:: python

    from pysbmy.field import read_field
    F=read_field("field.h5")
    F.write("field.h5")

    
FourierGrid and PowerSpectrum structures
----------------------------------------

These structures are used for cosmological power spectra. The entire Fourier grid of the simulation box is stored. The PowerSpectrum structure is a super-structure of FourierGrid.

Content
~~~~~~~

The HDF5 file structure for a FourierGrid is:

* ``/info/scalars/L0`` (attribute, type ``double``): size of the box in Mpc/h
* ``/info/scalars/L1`` (attribute, type ``double``): size of the box in Mpc/h
* ``/info/scalars/L2`` (attribute, type ``double``): size of the box in Mpc/h
* ``/info/scalars/N0`` (attribute, type ``int``): number of volume elements of the mesh
* ``/info/scalars/N1`` (attribute, type ``int``): number of volume elements of the mesh
* ``/info/scalars/N2`` (attribute, type ``int``): number of volume elements of the mesh
* ``/info/scalars/N2_HC`` (attribute, type ``int``): number of modes in the half-complex Fourier space in the z-direction (:math:`\mathrm{N2\_HC}=(\mathrm{N2}+1)/2`; ``N2`` should be a multiple of 2)
* ``/info/scalars/N_HC`` (attribute, type ``int``): number of modes in the half-complex Fourier space (:math:`\mathrm{N\_HC}=\mathrm{N0} \times \mathrm{N1} \times \mathrm{N2\_HC}`)
* ``/info/scalars/NUM_MODES`` (attribute, type ``int``): number of k modes
* ``/info/scalars/kmax`` (attribute, type ``double``): maximum wavenumber
* ``/info/scalars/k_keys`` (attribute, type ``int``, dimensions (``N0``, ``N1``, ``N2_HC``)): the keys to be used as a function of the Fourier mode (:math:`\mathrm{k\_keys}[i][j][k]=b`)
* ``/info/scalars/k_modes`` (attribute, type ``4-byte float little-endian`` (``<f4``) dimension ``NUM_MODES``): value of the wavenumber for this key (:math:`k[b]`)
* ``/info/scalars/k_nmodes`` (attribute, type ``int``, dimension ``NUM_MODES``): number of modes for this key (:math:`N_k[b]`)

A PowerSpectrum structure contains additionally:

* ``/scalars/powerspectrum`` (dataset, type ``4-byte float little-endian`` (``<f4``) dimension ``NUM_MODES``): value of the power spectrum for this key (:math:`P[b]`)

Use in C
~~~~~~~~

In Simbelmynë C files, a FourierGrid structure can be read, written and freed using respectively:

.. code-block:: C

    FourierGrid G = read_FourierGrid("fouriergrid.h5")
    write_FourierGrid(G, "fouriergrid.h5");
    free_FourierGrid(G);

and similarly for a PowerSpectrum structure:

.. code-block:: C

    PowerSpectrum P = read_PowerSpectrum("powerspectrum.h5")
    write_PowerSpectrum(P, "powerspectrum.h5");
    free_PowerSpectrum(P);


Use in python
~~~~~~~~~~~~~

With python, a FourierGrid structure can be read/written using:

.. code-block:: python

    from pysbmy.fft import read_FourierGrid
    G=read_FourierGrid("fouriergrid.h5")
    G.write("fouriergrid.h5")

and a PowerSpectrum structure using:

.. code-block:: python

    from pysbmy.power import read_powerspectrum
    P=read_powerspectrum("powerspectrum.h5")
    P.write("powerspectrum.h5")


SurveyGeometry and GalaxySelectionWindow structures
---------------------------------------------------

These structures are used for survey geometry and selection windows. 

Content
~~~~~~~

For both SurveyGeometry and GalaxySelectionWindow structures, HDF5 files contain:

* ``/info/scalars/L0`` (attribute, type ``double``): size of the box in Mpc/h
* ``/info/scalars/L1`` (attribute, type ``double``): size of the box in Mpc/h
* ``/info/scalars/L2`` (attribute, type ``double``): size of the box in Mpc/h
* ``/info/scalars/corner0`` (attribute, type ``double``): x-position in Mpc/h of the corner of the box with respect to the observer (which is at (0,0,0)).
* ``/info/scalars/corner1`` (attribute, type ``double``): y-position in Mpc/h of the corner of the box with respect to the observer (which is at (0,0,0)).
* ``/info/scalars/corner2`` (attribute, type ``double``): z-position in Mpc/h of the corner of the box with respect to the observer (which is at (0,0,0)).
* ``/info/scalars/N0`` (attribute, type ``int``): number of volume elements of the mesh
* ``/info/scalars/N1`` (attribute, type ``int``): number of volume elements of the mesh
* ``/info/scalars/N2`` (attribute, type ``int``): number of volume elements of the mesh
* ``/info/scalars/N_COSMOPAR`` (attribute, type ``int``): number of cosmological parameters stored
* ``/info/scalars/cosmo`` (attribute, type ``double``, dimension ``N_COSMOPAR``): cosmological parameters stored in the default order of Simbelmynë (see ``pysbmy/cosmology.py``)

SurveyGeometry structures contain additionally:

* ``/info/scalars/bright_cut`` (attribute, type ``double``, dimension ``N_CAT``): lower value of the cuts in magnitude in each subcatalog
* ``/info/scalars/faint_cut`` (attribute, type ``double``, dimension ``N_CAT``): upper value of the cuts in magnitude in each subcatalog
* ``/info/scalars/rmin`` (attribute, type ``double``, dimension ``N_CAT``): minimum comoving distance of objects in each subcatalog
* ``/info/scalars/rmax`` (attribute, type ``double``, dimension ``N_CAT``): maximum comoving distance of objects in each subcatalog
* ``/info/scalars/zmin`` (attribute, type ``double``, dimension ``N_CAT``): minimum redshift of objects in each subcatalog
* ``/info/scalars/zmax`` (attribute, type ``double``, dimension ``N_CAT``): maximum redshift of objects in each subcatalog
* ``/info/scalars/N_BIAS`` (attribute, type ``int``): number of bias parameters
* ``/info/scalars/galaxy_bias_mean`` (attribute, type ``double``, dimensions (``N_CAT``, ``N_BIAS``)): mean values of bias parameters in each subcatalog
* ``/info/scalars/galaxy_bias_std`` (attribute, type ``double``, dimensions (``N_CAT``, ``N_BIAS``)): standard deviations of bias parameters in each subcatalog
* ``/info/scalars/galaxy_nmean_mean`` (attribute, type ``double``, dimension ``N_CAT``): mean value of the expected number of galaxies in each subcatalog
* ``/info/scalars/galaxy_nmean_std`` (attribute, type ``double``, dimension ``N_CAT``): standard deviation of the expected number of galaxies in each subcatalog
* ``/info/scalars/N_CAT`` (attribute, type ``int``): number of subcatalogs
* ``/scalars/galaxy_sel_window_{ICAT``} (dataset, type ``4-byte float little-endian`` (``<f4``), dimensions (``N0``, ``N1``, ``N2``)) for :math:`0 \leq \mathrm{ICAT} < \mathrm{N\_CAT}`: the 3D galaxy selection window for each subcatalog

GalaxySelectionWindow structures contain additionally:

* ``/info/scalars/bright_cut`` (attribute, type ``double``): lower value of the cuts in magnitude in each subcatalog
* ``/info/scalars/faint_cut`` (attribute, type ``double``): upper value of the cuts in magnitude in each subcatalog
* ``/info/scalars/rmin`` (attribute, type ``double``): minimum comoving distance of objects in each subcatalog
* ``/info/scalars/rmax`` (attribute, type ``double``): maximum comoving distance of objects in each subcatalog
* ``/info/scalars/zmin`` (attribute, type ``double``): minimum redshift of objects in each subcatalog
* ``/info/scalars/zmax`` (attribute, type ``double``): maximum redshift of objects in each subcatalog
* ``/info/scalars/N_BIAS`` (attribute, type ``int``): number of bias parameters
* ``/info/scalars/galaxy_bias_mean`` (attribute, type ``double``, dimension ``N_BIAS``): mean values of bias parameters in each subcatalog
* ``/info/scalars/galaxy_bias_std`` (attribute, type ``double``, dimension ``N_BIAS``): standard deviations of bias parameters in each subcatalog
* ``/info/scalars/galaxy_nmean_mean`` (attribute, type ``double``): mean value of the expected number of galaxies in each subcatalog
* ``/info/scalars/galaxy_nmean_std`` (attribute, type ``double``): standard deviation of the expected number of galaxies in each subcatalog
* ``/scalars/galaxy_sel_window_{ICAT``} if ``ICAT`` is defined, or ``/scalars/field`` (dataset, type ``4-byte float little-endian`` (``<f4``), dimensions (``N0``, ``N1``, ``N2``)): the 3D galaxy selection window

Use in C
~~~~~~~~

C routines to read and free SurveyGeometry and GalaxySelectionWindow structures are available and can be used as follows:

.. code-block:: C

    SurveyGeometry SG = read_survey_geometry_header("survey_geometry.h5");
    free_survey_geometry_header(SG);
    GalaxySelectionWindow GSW = read_galaxy_selection_window("galaxy_sel_window.h5", ICAT);
    free_galaxy_selection_window(GSW);

``read_galaxy_selection_window`` will only work if ``ICAT`` is set in the file to be read.

Use in python
~~~~~~~~~~~~~

With python, a SurveyGeometry structure can be read/written using:

.. code-block:: python

    from pysbmy.survey_geometry import read_survey_geometry
    SG=read_survey_geometry("survey_geometry.h5");
    SG.write("survey_geometry.h5");

and a GalaxySelectionWindow structure using:

.. code-block:: python

    from pysbmy.survey_geometry import read_galaxy_sel_window
    GSW=read_galaxy_sel_window("galaxy_sel_window.h5")
    GSW.write("galaxy_sel_window.h5")

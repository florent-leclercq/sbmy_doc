Parameter file
==============

The **configuration file for Simbelmynë** is an ASCII text file. The recommended extension is ``.sbmy``. A template is provided below, and it can also be generated using python.


Template
--------

.. code-block:: python

    ## ----------------------------------------------------------------------------------------------------------------------- ##
    ## -----------------------------      _           _          _                             ------------------------------- ##
    ## -----------------------------     (_)         | |        | |                            ------------------------------- ##
    ## -----------------------------  ___ _ _ __ ___ | |__   ___| |_ __ ___  _   _ _ __   ___  ------------------------------- ##
    ## ----------------------------- / __| | '_ ` _ \| '_ \ / _ \ | '_ ` _ \| | | | '_ \ / _ \ ------------------------------- ##
    ## ----------------------------- \__ \ | | | | | | |_) |  __/ | | | | | | |_| | | | |  __/ ------------------------------- ##
    ## ----------------------------- |___/_|_| |_| |_|_.__/ \___|_|_| |_| |_|\__, |_| |_|\___| ------------------------------- ##
    ## -----------------------------                                          __/ |            ------------------------------- ##
    ## -----------------------------                                         |___/             ------------------------------- ##
    ## ----------------------------------------------------------------------------------------------------------------------- ##

    ## ----------------------------------------------------------------------------------------------------------------------- ##
    ## Setup ----------------------------------------------------------------------------------------------------------------- ##
    ## ----------------------------------------------------------------------------------------------------------------------- ##
    SnapFormat                                        3
    NumFilesPerSnapshot                               1

    ## ----------------------------------------------------------------------------------------------------------------------- ##
    ## Module LPT ------------------------------------------------------------------------------------------------------------ ##
    ## ----------------------------------------------------------------------------------------------------------------------- ##
    ModuleLPT                                         1
    InputRngStateLPT                                  0
    # Set this to '0' to reset the random number generator
    OutputRngStateLPT                                 dummy.rng

    # Basic setup: -------------------------
    Particles                                         128
    # Number of particles in each dimension of the initial grid
    Mesh                                              128
    # The grid on which densities are computed
    BoxSize                                           200.0
    corner0                                           -100.0
    corner1                                           -100.0
    corner2                                           -100.0

    # Initial conditions: ------------------
    ICsMode                                           0
    # 0 : the codes generates white noise, then initial conditions
    # 1 : external white noise specified, the code multiplies by the power spectrum
    # 2 : external initial conditions specified
    WriteICsRngState                                  0
    OutputICsRngState                                 dummy.rng
    WriteWhiteNoise                                   0
    OutputWhiteNoise                                  initial_density_white.h5
    # Only used if ICsMode=0
    InputWhiteNoise                                   initial_density_white.h5
    # Only used if ICsMode=1
    InputInitialConditions                            initial_density.h5
    # Only used if ICsMode=2
    WriteInitialConditions                            0
    OutputInitialConditions                           initial_density.h5
    # At a scale factor of a=1e-3

    # Power spectrum: ----------------------
    InputPowerSpectrum                                input_power.h5
    # Only used if ICsMode=0 or 1

    # Final conditions: --------------------
    RedshiftLPT                                       0.0
    WriteLPTSnapshot                                  1
    OutputLPTSnapshot                                 lpt_particles.gadget3
    WriteLPTDensity                                   1
    OutputLPTDensity                                  lpt_density.h5

    ## ----------------------------------------------------------------------------------------------------------------------- ##
    ## Module PM/COLA -------------------------------------------------------------------------------------------------------- ##
    ## ----------------------------------------------------------------------------------------------------------------------- ##
    ModulePMCOLA                                      0
    EvolutionMode                                     2
    # 1 : PM
    # 2 : COLA
    ParticleMesh                                      128
    # The grid used in the particle-mesh code
    NumberOfTimeSteps                                 10
    TimeStepDistribution                              0
    # 0 : linear in the scale factor
    # 1 : logarithmic in the scale factor
    # 2 : exponential in the scale factor
    ModifiedDiscretization                            1
    # Use modified discretization of Kick and Drift operators in COLA
    n_LPT                                             -2.5
    # Exponent for the Ansatz in the modified Kick and Drift operators in COLA, only used if ModifiedDiscretization=1

    # Intermediate snapshots: --------------
    WriteSnapshots                                    0
    OutputSnapshotsBase                               particles_
    OutputSnapshotsExt                                .gadget3
    WriteDensities                                    0
    OutputDensitiesBase                               density_
    OutputDensitiesExt                                .h5

    # Final snapshot: ----------------------
    RedshiftFCs                                       0.0
    WriteFinalSnapshot                                0
    OutputFinalSnapshot                               final_particles.gadget3
    WriteFinalDensity                                 1
    OutputFinalDensity                                final_density.h5

    ## ----------------------------------------------------------------------------------------------------------------------- ##
    ## Module RSD ------------------------------------------------------------------------------------------------------------ ##
    ## ----------------------------------------------------------------------------------------------------------------------- ##
    ModuleRSD                                         0
    DoNonLinearMapping                                0
    vobs0                                             0.0
    vobs1                                             0.0
    vobs2                                             0.0
    # Velocity of the observer with respect to the CMB/galaxies frame
    alpha1RSD                                         1.0
    DoLPTSplit                                        0
    alpha2RSD                                         1.0
    # RSD model:
    # if doLPTSplit=false:
    # 	z_obs = z_cosmo + (1+z_cosmo) * alpha1RSD * z_pec
    # else:
    # 	z_obs = z_cosmo + (1+z_cosmo) * (alpha1RSD * z_pec_LPT + alpha2RSD * (z_pec-z_pec_LPT))
    # alpha1RSD=alpha2RSD=1 in LCDM

    # Reshift-space snapshot: --------------
    WriteRSSnapshot                                   0
    OutputRSSnapshot                                  rs_particles.gadget3
    WriteRSDensity                                    1
    OutputRSDensity                                   rs_density.h5

    ## ----------------------------------------------------------------------------------------------------------------------- ##
    ## Module Mock Catalogs -------------------------------------------------------------------------------------------------- ##
    ## ----------------------------------------------------------------------------------------------------------------------- ##
    ModuleMocks                                       0
    InputRngStateMocks                                0
    # Set this to '0' to reset the random number generator
    OutputRngStateMocks                               dummy.rng
    WriteMocksRngState                                0
    OutputMocksRngState                               dummy.rng
    NoiseModel                                        1
    # 0 : Gaussian linear model
    # 1 : Poisson model
    NumberOfNoiseRealizations                         5
    # Should be about NumberOfkBins^2 for a Poisson noise model

    # Inputs: ------------------------------
    InputDensityMocks                                 lpt_density.h5
    # Only used if ModuleLPT is switched off, uses final density produced by LPT/PM/COLA otherwise
    InputSurveyGeometry                               input_survey_geometry.h5
    InputSummaryStatskGrid                            input_ss_k_grid.h5
    # Only mandatory if WriteSummaryStats=1

    # Output mocks: ------------------------
    WriteMocks                                        1
    OutputMockBase                                    output_mock_
    OutputMockExt                                     .h5

    # Output summaries: --------------------
    WriteSummaryStats                                 1
    OutputSummaryStats                                output_ss.h5

    ## ----------------------------------------------------------------------------------------------------------------------- ##
    ## Cosmological model ---------------------------------------------------------------------------------------------------- ##
    ## ----------------------------------------------------------------------------------------------------------------------- ##
    # Planck 2015 cosmological parameters (Planck 2015 XIII, p31, table 4, last column)
    # The equation of state of dark energy is parametrized by w(a) = w0_fld + (1-a)/a0 * wa_fld
    h                                                 0.6774
    Omega_r                                           0.0
    Omega_q                                           0.6911
    Omega_b                                           0.0486
    Omega_m                                           0.3089
    Omega_k                                           0.0
    n_s                                               0.9667
    sigma8                                            0.8159
    w0_fld                                            -1.0
    wa_fld                                            0.0


.. _`use the python generator`:

Generation with python
----------------------

The most convenient and recommended way to generate a Simbelmynë parameter file is using python. 

The python class ``param_file`` defined in ``pysbmy/pysbmy.py`` represents a Simbelmynë parameter file. An instance can be created and written on the disk, using (for example):

.. code-block:: python

    from pysbmy import param_file
    S=param_file(Particles=128, Mesh=128, BoxSize=250, corner0=-125, corner1=-125, corner2=-125, WriteInitialConditions=1)
    S.write("config.sbmy")

Any parameter that is not defined when creating an instance of the class ``param_file`` is set to its default value.


Description and default values
------------------------------

Setup
~~~~~

* ``SnapFormat`` (default ``3``): Snapshot format, using standards of the Gadget simulation code. See the |gadgetmanual| for a description. Accepted values: 1 (Gadget file format 1), 2 (Gadget file format 2), 3 (Gadget HDF file format).
* ``NumFilesPerSnapshot`` (default ``1``): Number of files per snapshot.

.. |gadgetmanual| raw:: html

   <a href="https://wwwmpa.mpa-garching.mpg.de/gadget/users-guide.pdf" target="blank">user guide for GADGET</a>

Module LPT
~~~~~~~~~~

* ``ModuleLPT`` (default ``1``): Switch ON/OFF the module LPT. Accepted values: 0 or 1.
* ``InputRngStateLPT`` (default ``0``): Path to the input random number generator state (before module LPT is executed). Set this to '0' to reset the random number generator
* ``OutputRngStateLPT`` (default ``dummy.rng``): Path to the output random number generator state (after module LPT is executed).

Basic setup
"""""""""""

* ``Particles`` (default ``128``): Number of particles in each dimension of the initial grid
* ``Mesh`` (default ``128``): Number of points in each dimension of the grid on which densities are computed
* ``BoxSize`` (default ``200.0``): Box size in Mpc/h.
* ``corner0`` (default ``-100.0``): x-position in Mpc/h of the corner of the box with respect to the observer (which is at (0,0,0)).
* ``corner1`` (default ``-100.0``): y-position in Mpc/h of the corner of the box with respect to the observer (which is at (0,0,0)).
* ``corner2`` (default ``-100.0``): z-position in Mpc/h of the corner of the box with respect to the observer (which is at (0,0,0)).

Initial conditions
""""""""""""""""""

* ``ICsMode`` (default ``0``): Switch for the initial conditions mode. Accepted values: 0, 1, 2. 0 : the codes generates white noise, then initial conditions. 1 : external white noise specified, the code multiplies by the power spectrum. 2 : external initial conditions specified.
* ``WriteICsRngState`` (default ``0``): Switch ON/OFF to write the random number generator state before generation of the initial conditions. Accepted values: 0 or 1.
* ``OutputICsRngState`` (default ``dummy.rng``): Path to the random number generator state before generation of the initial conditions, if ``WriteICsRngState`` is ``1``.
* ``WriteWhiteNoise`` (default ``0``): Switch ON/OFF to write the white noise field.
* ``OutputWhiteNoise`` (default ``initial_density_white.h5``): Path to the output white noise field (only used if ``ICsMode`` is ``0`` and ``WriteWhiteNoise`` is ``1``).
* ``InputWhiteNoise`` (default ``initial_density_white.h5``): Path to the input white noise field (only used if ``ICsMode`` is ``1``).
* ``InputInitialConditions`` (default ``initial_density.h5``): Path to the input initial conditions (only used if ``ICsMode`` is ``2``).
* ``WriteInitialConditions`` (default ``0``): Switch ON/OFF to write the initial conditons, at a scale factor of  :math:`a=10^{-3}`.
* ``OutputInitialConditions`` (default ``initial_density.h5``): Path to the output initial conditions (only used if ``WriteInitialConditions`` is ``1`` and ``WriteInitialConditions`` is ``1``).

Power spectrum
""""""""""""""

* ``InputPowerSpectrum`` (default: ``input_power.h5``): Path to the input power spectrum, in Simbelmynë standard HDF5 file format. Only used if ``ICsMode`` is ``0`` or ``1``.

Final conditions
""""""""""""""""

* ``RedshiftLPT`` (default: ``0.0``): Redshift to which particles/densities are evolved after module LPT.
* ``WriteLPTSnapshot`` (default: ``1``): Switch ON/OFF to write the snapshot after module LPT.
* ``OutputLPTSnapshot`` (default: ``lpt_particles.gadget3``): Path to the snapshot after module LPT, if ``WriteLPTSnapshot`` is ``1``.
* ``WriteLPTDensity`` (default: ``1``): Switch ON/OFF to write the density field after module LPT.
* ``OutputLPTDensity`` (default: ``lpt_density.h5``): Path to the snapshot after module LPT, if ``WriteLPTDensity`` is ``1``.

Module PM/COLA
~~~~~~~~~~~~~~

* ``ModulePMCOLA`` (default: ``0``): Switch ON/OFF the module PM/COLA. Accepted values: 0 or 1.
* ``EvolutionMode`` (default: ``2``): Evolution mode. Accepted values: 1 or 2. 1 : PM. 2 : COLA.
* ``ParticleMesh`` (default: ``128``): Number of points in each dimension of the grid used in the particle-mesh code.
* ``NumberOfTimeSteps`` (default: ``10``): Number of timesteps.
* ``TimeStepDistribution`` (default: ``0``): Timesteps distribution. Accepted values: 0, 1, 2. 0 : linear in the scale factor. 1 : logarithmic in the scale factor. 2 : exponential in the scale factor.
* ``ModifiedDiscretization`` (default: ``1``): Switch ON/OFF to use the modified discretization of Kick and Drift operators in COLA. For details see Appendix A in |tCOLApaper|. Accepted values: 0 or 1.
* ``n_LPT`` (default: ``-2.5``): Exponent for the Ansatz in the modified Kick and Drift operators in COLA, only used if ``ModifiedDiscretization`` is ``1``.

.. |tCOLApaper| raw:: html

   <a href="https://arxiv.org/abs/1301.0322" target="blank">Tassev, Zaldarriaga & Eisenstein 2013</a>

Intermediate snapshots
""""""""""""""""""""""

* ``WriteSnapshots`` (default: ``0``): Switch ON/OFF to write intermediate snapshots. Accepted values: 0 or 1.
* ``OutputSnapshotsBase`` (default: ``particles_``): Base of the output snapshots filename, if ``WriteSnapshots`` is ``1``.
* ``OutputSnapshotsExt`` (default: ``.gadget3``): Extension of the output snapshots filename, if ``WriteSnapshots`` is ``1``.
* ``WriteDensities`` (default: ``0``):  Switch ON/OFF to write intermediate density fields. Accepted values: 0 or 1.
* ``OutputDensitiesBase`` (default: ``density_``): Base of the output density fields filename, if ``WriteDensities`` is ``1``.
* ``OutputDensitiesExt`` (default: ``.h5``): Extension of the output density fields snapshot filename, if ``WriteDensities`` is ``1``.

Final snapshot
""""""""""""""

* ``RedshiftFCs`` (default: ``0.0``): Redshift to which particles/densities are evolved after module PM/COLA.
* ``WriteFinalSnapshot`` (default: ``0``): Switch ON/OFF to write the final snapshot. Accepted values: 0 or 1.
* ``OutputFinalSnapshot`` (default: ``final_particles.gadget3``): Path to the output final snapshot, if ``WriteFinalSnapshot`` is ``1``.
* ``WriteFinalDensity`` (default: ``1``): Switch ON/OFF to write the final density field. Accepted values: 0 or 1.
* ``OutputFinalDensity`` (default: ``final_density.h5``): Path to the output final density field, if ``WriteFinalDensity`` is ``1``.

Module RSD
~~~~~~~~~~

* ``ModuleRSD`` (default: ``0``): Switch ON/OFF the module RSD. Accepted values: 0 or 1.
* ``DoNonLinearMapping`` (default: ``0``): Switch ON/OFF to do the non-linear redshift-space distortions mapping. Accepted values: 0 or 1.
* ``vobs0`` (default: ``0.0``): x-velocity of the observer with respect to the CMB/galaxies frame, in km/s.
* ``vobs1`` (default: ``0.0``): y-velocity of the observer with respect to the CMB/galaxies frame, in km/s.
* ``vobs2`` (default: ``0.0``): z-velocity of the observer with respect to the CMB/galaxies frame, in km/s.
* ``alpha1RSD`` (default: ``1.0``): Value of :math:`\alpha_1` (should be 1 in LCDM).
* ``DoLPTSplit`` (default: ``0``): Switch ON/OFF to do split with the LPT velocity. Accepted values: 0 or 1.
* ``alpha2RSD`` (default: ``1.0``): Value of :math:`\alpha_2` (should be 1 in LCDM). Equivalent to ``DoLPTSplit=0`` if :math:`\alpha_2 = \alpha_1`.

The RSD model is the following:

* if ``DoNonLinearMapping=0`` and ``doLPTSplit=0``: :math:`\textbf{s} = \textbf{r} + \alpha_1 \times (\textbf{v} \cdot \textbf{r}) \times \textbf{r} /(H |\textbf{r}|^2)`.
* if ``DoNonLinearMapping=0`` and ``doLPTSplit=1``: :math:`\textbf{s} = \textbf{r} + \alpha_1 \times (\textbf{v}_\mathrm{LPT} \cdot \textbf{r}) \times \textbf{r} /(H |\textbf{r}|^2) + \alpha_2 \times ((\textbf{v}-\textbf{v}_\mathrm{LPT}) \cdot \textbf{r}) \times \textbf{r} /(H |\textbf{r}|^2)`.
* if ``DoNonLinearMapping=1`` and ``doLPTSplit=0``: :math:`z_\mathrm{obs} = z_\mathrm{cosmo} + (1+z_\mathrm{cosmo}) \times \alpha_1 \times z_\mathrm{pec}`.
* if ``DoNonLinearMapping=1`` and ``doLPTSplit=1``: :math:`z_\mathrm{obs} = z_\mathrm{cosmo} + (1+z_\mathrm{cosmo}) \times (\alpha_1 \times z_\mathrm{pec,LPT} + \alpha_2 \times (z_\mathrm{pec}-z_\mathrm{pec,LPT}))`.

Reshift-space snapshot
""""""""""""""""""""""

* ``WriteRSSnapshot`` (default: ``0``): Switch ON/OFF to write the redshift-space snapshot. Accepted values: 0 or 1.
* ``OutputRSSnapshot`` (default: ``rs_particles.gadget3``): Path to the output redshift-space snapshot, if ``WriteRSSnapshot`` is ``1``.
* ``WriteRSDensity`` (default: ``1``): Switch ON/OFF to write the redshift-space density field. Accepted values: 0 or 1.
* ``OutputRSDensity`` (default: ``rs_density.h5``): Path to the output redshift-space density field, if ``WriteRSDensity`` is ``1``.

Module Mock Catalogs
~~~~~~~~~~~~~~~~~~~~

* ``ModuleMocks`` (default: ``0``): Switch ON/OFF the module Mock Catalogs. Accepted values: 0 or 1.
* ``InputRngStateMocks`` (default: ``0``): Path to the input random number generator state (before module Mock Catalogs is executed). Set this to '0' to reset the random number generator 
* ``OutputRngStateMocks`` (default: ``dummy.rng``): Path to the output random number generator state (after module Mock Catalogs is executed).
* ``WriteMocksRngState`` (default: ``0``):  Switch ON/OFF to write the random number generator state before generation of mock catalogs. Accepted values: 0 or 1.
* ``OutputMocksRngState`` (default ``dummy.rng``): Path to the random number generator state before generation of mock catalogs, if ``WriteMocksRngState`` is ``1``.
* ``NoiseModel`` (default: ``1``): Noise model. Accepted values: 0, 1. 0 : Gaussian linear model. 1 : Poisson model.
* ``NumberOfNoiseRealizations`` (default: ``5``): Number of noise realizations desired. Should be of order number of k-bins squared for a Poisson noise model.

Inputs
""""""

* ``InputDensityMocks`` (default: ``lpt_density.h5``): Path to the input density for mock catalogs. Only used if ModuleLPT is switched off, uses final density produced by LPT/PM/COLA otherwise.
* ``InputSurveyGeometry`` (default: ``input_survey_geometry.h5``): Path to the input survey geometry. 
* ``InputSummaryStatskGrid`` (default: ``input_ss_k_grid.h5``): Path to the input k-grid for the output summary statistics. Only mandatory if ``WriteSummaryStats`` is ``1``.

Output mocks
""""""""""""

* ``WriteMocks`` (default: ``1``): Switch ON/OFF to write the mock catalogs. Accepted values: 0 or 1.
* ``OutputMockBase`` (default: ``output_mock_``): Base of the output mock catalogs filename, if ``WriteMocks`` is ``1``.
* ``OutputMockExt`` (default: ``.h5``): Extension of the output mock catalogs filename, if ``WriteMocks`` is ``1``.

Output summaries
""""""""""""""""

* ``WriteSummaryStats`` (default: ``1``): Switch ON/OFF to write summary statistics. Accepted values: 0 or 1.
* ``OutputSummaryStats`` (default: ``output_ss.h5``): Path to the output summary statistics, if ``WriteSummaryStats`` is ``1``.

Cosmological model
~~~~~~~~~~~~~~~~~~

The cosmological model. Default values are Planck 2015 cosmological parameters (see |planckcosmo|, page 32, table 4, last column). The equation of state of dark energy is parametrized (as in CLASS) by :math:`w(a) = w_0 + (1-a)/a_0 \times w_a`.

.. |planckcosmo| raw:: html

   <a href="https://arxiv.org/abs/1502.01589" target="blank">Planck 2015 results. XIII. Cosmological parameters</a>

* ``h`` (default: ``0.6774``): Hubble constant
* ``Omega_r`` (default: ``0.0``): radiation density
* ``Omega_q`` (default: ``0.6911``): dark energy density
* ``Omega_b`` (default: ``0.0486``): baryon density
* ``Omega_m`` (default: ``0.3089``): matter density
* ``Omega_k`` (default: ``0.0``): curvature density
* ``n_s`` (default: ``0.9667``): spectral index
* ``sigma8`` (default: ``0.8159``): :math:`\sigma_8`
* ``w0_fld`` (default: ``-1.0``): dark energy equation of state
* ``wa_fld`` (default: ``0.0``): dark energy equation of state


Known issue
-----------

Sometimes when the ASCII template is used and/or the parameter file is modified "by hand" using certain text editors (among which vim), Simbelmynë will produce an error when parsing:

.. code-block:: console

    [XX:XX:XX|STATUS    ]|Reading parameter file in 'config.sbmy'...
    [XX:XX:XX|ERROR     ]==|Error in utils.c:p_fgets:197: Incorrect reading in read_param.c:read_parameterfile:603.
    [XX:XX:XX|ERROR     ]==|Error in utils.c:p_fgets:199: Attempting to read: .
    [XX:XX:XX|ERROR     ]==|Error in utils.c:p_fgets:200: I/O error (fgets)

This seems to be linked to end-of-line characters and will be later fixed. For the moment, the most convenient way to bypass this problem is to `use the python generator`_.
